const config = require('config');
const http = require('http');
const path = require('path');
const express = require('express');
const mongoose = require('mongoose');
const helmet = require('helmet');
const cors = require('cors');
const fileUpload = require('express-fileupload');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const colors = require('colors');
const app = express(); // server api

// ------ Load Routes -------
const filesRouter = require('../routes/files');
const adminRouter = require('../routes/admin');
const chatRouter = require('../routes/chat');
const usersRouter = require('../routes/users');
const flightServicesRouter = require('../routes/flightservices');
const chatServices = require('../services/chatService');

// ------ Create Server on port 2000 -------
const server = http.createServer(app);
const port = process.env.PORT || 2000;
server.listen(port);

server.on('listening', () => {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? `pipe ${addr}`
        : `port ${addr.port}`;
    console.log(`Listening on ${bind} - FLYVIEW server`);
});

// ------ Configuration -------
console.log(colors.green('Application name: ' + config.get('name')));
console.log(colors.green('Mail Server:  ' + config.get('mail.host')));

app.set('view engine', 'pug');
app.set('views', './views');

if (app.get('env') === 'development') {
    app.use(morgan('dev'));
    console.log('Morgan enabled...');
}

// ------ Connecting to DataBase -------
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.connect('mongodb://localhost/flyview', {useNewUrlParser: true, useUnifiedTopology: true})
    .then(() => console.log(colors.green('Connected to MongoDB')))
    .catch(err => console.log(colors.red('Could not connect to MongoDB...')));

// ------ Middleware functions -------
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cookieParser());
app.use(helmet());
app.use(cors());
app.use(fileUpload());

app.use((req, res, next) => {
    console.log(colors.green(`entering request: ${req.originalUrl}`));
    next();
});

// ------ Set Routes -------
app.use('/api/files', filesRouter);
app.use('/api/chat', chatRouter);
app.use('/api/admin', adminRouter);
app.use('/api/users', usersRouter);
app.use('/api/flight', flightServicesRouter);

// ------ Set Chat Service on Port 2001 --------
const io = require('socket.io')(server);
chatServices.startListenChat({io: io, port: 2001});

// ------ Welcome massage -------
app.get('/', (req, res) => {
        res.render('start', {title: 'Welcome', message: 'Hello Fly-View'});
        // res.send("Hello Fly-View")
    }
);

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    console.error(`error in express`, err)
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});



