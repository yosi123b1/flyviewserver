const mongoose = require('mongoose');
const Joi = require('joi');

const fileSchema = new mongoose.Schema({
    name: String,
    userId: String,
    screenId: String,
    path: String,
});

const FileData = new mongoose.model('File', fileSchema);

// ***Validate file***
function validateFile(file) {
    const schema = Joi.object({
        name: Joi.string().min(3).required(),
        data: Joi.optional(),
        size: Joi.number().required(),
        encoding: Joi.optional(),
        tempFilePath: Joi.optional(),
        truncated: Joi.optional(),
        mimetype: Joi.optional(),
        md5: Joi.optional(),
        mv: Joi.optional(),
    });
    const result = schema.validate(file);
    return result
}

// ***Validate file***
function validateUpdate(file) {
    const schema = Joi.object({
        name: Joi.string().min(3),
        data: Joi.optional(),
    });
    const result = schema.validate(file);
    return result
}

exports.FileData = FileData;
exports.validateFile = validateFile;
exports.validateUpdate = validateUpdate;
