const mongoose = require('mongoose');
const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const OrderData = new mongoose.model('Order', new mongoose.Schema({
    userId: String,
    screenId: String,
    timeStamp: Date,
    products: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    }]
}));

// ***Validate product***
function validateOrder(file) {
    const schema = Joi.object({
        userId: Joi.string().required(),
        screenId: Joi.string().required(),
        products: Joi.array()
            .items(Joi.objectId())
    });
    const result = schema.validate(file);
    return result
}

// ***Validate product***
function validateUpdatedOrder(file) {
    const schema = Joi.object({
        userId: Joi.string().required(),
        screenId: Joi.string().required(),
        products: Joi.array()
            .items(Joi.objectId()).optional()
    });
    const result = schema.validate(file);
    return result
}

exports.OrderData = OrderData;
exports.validateOrder = validateOrder;
exports.validateUpdatedOrder = validateUpdatedOrder;
