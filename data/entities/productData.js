const mongoose = require('mongoose');
const Joi = require('joi');

const ProductData = new mongoose.model('Product', new mongoose.Schema({
    name: String,
    price: Number,
    amount: Number,
    isAvailable: Boolean,
    description: String,
    imageUrl: String,
}));

// ***Validate product***
function validateProduct(file) {
    const schema = Joi.object({
        name: Joi.string().min(3).required(),
        price: Joi.number().optional(),
        amount: Joi.number().required(),
        isAvailable: Joi.bool().required(),
        description: Joi.string().optional(),
        imageUrl: Joi.string().optional(),

    });
    const result = schema.validate(file);
    return result
}

// ***Validate product***
function validateUpdatedProduct(file) {
    const schema = Joi.object({
        price: Joi.number().optional(),
        amount: Joi.number().optional(),
        isAvailable: Joi.bool().optional(),
        description: Joi.string().optional(),
        imageUrl: Joi.string().optional(),

    });
    const result = schema.validate(file);
    return result
}

exports.ProductData = ProductData;
exports.validateProduct = validateProduct;
exports.validateUpdatedProduct = validateUpdatedProduct;
