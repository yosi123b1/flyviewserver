const mongoose = require('mongoose');
const Joi = require('joi');

const ScreenData = new mongoose.model('Screen', new mongoose.Schema({
    screenId: {
        type: String,
        required : true,
        unique: true
    },
    userId:{
        type: String,
        required: false
    },
    screenCode: {
        type: String,
        required: true
    },
    connectRequest: Boolean,
    isConnected: Boolean
}));


exports.ScreenData = ScreenData;
