const mongoose = require('mongoose');
const userRole = require('../utils/userRole');
const Joi = require('joi');

const userData = new mongoose.model('User', new mongoose.Schema({
    userId: {
        type: String,
        unique: true,
        required: true
    },
    role: {
        type: String,
        enum: [userRole.admin, userRole.crew, userRole.passenger],
        default: userRole.passenger
    },
    name: String,
    screenId: String
}));

exports.UserData = userData;