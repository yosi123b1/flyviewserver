const userRole = {admin: 'admin', crew: 'crew', passenger: 'passenger'};

module.exports = userRole;