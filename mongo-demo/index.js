const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/playground').then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDVB ' + err.message)); //Create new db or connect to exist one

const courseSchema = new mongoose.Schema({ //Crete new schema - new class on the db
    name: {
        type: String,
        required: true,
        minlength: 5,
        maxlength: 2555
    },
    category:{
        type: String,
        required: true,
        enum: ['web', 'mobile', 'network']
    },
    author: String,
    tags: {
        type: Array,
        validate:{
            validator: function (v){
                return v && v.length > 0;
            },
            message: 'A course should have at least one tag.'
        }
    },
    data: {type: Date, default: Date.now()},
    isPublished: Boolean,
    price: {
        type: Number,
        min: 10,
        max: 200,
        required: function () {
            return this.isPublished;
        },
        get: v=> v, //Take cares the getter
        set: v=> Math.round(v) //Take cares the setter
    }
});

const Course = mongoose.model('Course', courseSchema); //insert collection named Courses by schema named courseSchema

async function createCourse() {
    const course = new Course({
        name: 'NodeJs',
        author: 'Yosi',
        tags: ['node', 'backend'],
        category: 'web',
        isPublished: false,
    });

    try {
        const res = await course.save(); // Save an object of type Course to the DB
        console.log(res);
    } catch (ex) {
        console.log(ex.message);
    }

}

async function getCourses() {
    // functions to complex methods - eq , ne, gt, gte, lt, lth, in, nin
    // let products = await Products
    // .find({price: {$gt: 10}}) // for example find product with price more then 10
    // .find({price: {$gt: 10, $lt: 20}}) // for example find product with price more then 10 and low then 20
    // .find({price: {$in: [10,20,30]}}) // for example find product with price in 10,20 or 30

    // Regular Expressions - start with..., end with..., contains...
    // let courses = await Course
    // .find({name: /^Yosi/ ) // starts with yosi
    // .find({name: /Bukris$/i ) // ends with bukris, 'i' - is esensitve
    // .find({name: /.*yosi.*/ ) // contains yosi in its name

    const pageNumber = 2;
    const pageSize = 2;

    const courses = await Course
        .find({name: 'Node'}) // Find by name, can pass more then one attribute
        .skip((pageNumber - 1) * pageSize) // Implements pagination!!!
        .limit(pageSize) // Limit to page size of the pagination
        .sort({name: 1}) // sort by criteria and side - 1 is asc and -1 is dec
        .count() // presents only the num of objects in the criteria
        .select({name: 1, tags: 1}); // Can decide what properties you want to show
    console.log(courses);
}

async function updateCourse(id) {
    // Find and update
    let courseV1 = await Course.findById(id);
    if (!courseV1) return;

    courseV1.set({
        isPublished: false,
        author: 'Yosi'
    });
    const res = await courseV1.save();
    console.log(res);

    // Update and find
    const courseV2 = await Course.findByIdAndUpdate(id, {
        $set: {
            author: 'jack',
            isPublished: true
        }
    }, {new: true})
    console.log(courseV2)
}

async function removeCourse(id) {
    // remove one object, if given criteria - delete the first one
    // Course.deleteOne({isPublished: false})

    await Course.deleteOne({_id: id}).then(res => console.log(res));
    // await Course.findByIdAndRemove(id).then(res => console.log(res));

    // To remove multiple objects
    // await Course.deleteMany({_id: id}).then(res => console.log(res));
}

createCourse();