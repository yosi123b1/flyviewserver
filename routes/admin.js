const express = require('express');
const router = express.Router();
const userRole = require('../data/utils/userRole');
const usersService = require('../services/usersService');
const filesService = require('../services/filesService');
const ordersService = require('../services/ordersService');
const productsService = require('../services/productsService');
const colors = require('colors');

// ------ API GET METHODS -------
// ***Get all conversations***
router.get('/conversations/:flightID', (req, res) => {
        res.send('***Get all conversations***')
    }
);

// ***Get all users***
router.get('/users/:userId', async (req, res) => {
        await usersService.getAllUsers(req, res)
    }
);

// ***Get all users***
router.get('/screens/:userId', async (req, res) => {
        await usersService.getAllScreens(req, res);
    }
);

// ***Get all files***
router.get('/files/:userId', async (req, res) => {
        let role = await usersService.getUserRole({userId: req.params.userId})
        if (role != userRole.admin)
            return res.status(400).json({message: "user role must be admin for this action"})
        await filesService.getAllFiles(req, res);
    }
);

// ***Get all flight orders***
router.get('/orders/:userId', async (req, res) => {
        await ordersService.getAllOrders(req, res);
    }
);

// ------ API POST METHODS -------
// ***Create New User***
router.post('/users',async (req, res) => {
        const user = await usersService.createUser({role: req.body.role, name: req.body.name})
        if (!user)
            return res.status(500).json({message:"failed to create user"})
        return res.send(user)
    }
);

// ------ API DELETE METHODS -------
// ***Delete all files***
router.delete('/files/:userId', async (req, res) => {
        let role = await usersService.getUserRole({userId: req.params.userId})
        if (role != userRole.admin)
            return res.status(400).json({message: "user role must be admin for this action"})
        await filesService.deleteAllFiles(req, res)
    }
);

// ***Delete all conversations***
router.delete('/conversations', (req, res) => {
        res.send('***Delete all conversations***')
    }
);

// ***Delete all users***
router.delete('/users/:userId', async (req, res) => {
        await usersService.disconnectAllUsers(req, res)
    }
);

// ***Delete all orders***
router.delete('/orders/:userId', async (req, res) => {
        await ordersService.deleteAllOrders(req, res)
    }
);

// ***Delete all products***
router.delete('/products/:userId', async (req, res) => {
        await productsService.deleteAllProducts(req, res)
    }
);

module.exports = router;