const express = require('express');
const router = express.Router();
const Joi = require('joi'); // help us to ensure that all the parameters comes as we need

// ------ DATA -------

// ------ API GET METHODS -------
// ***Get conversation***
router.get('/:id', (req, res) => {
        res.send('***Get conversation***')
    }
);

// ------ API POST METHODS -------
// ***Open new Conversation***
router.post('/', (req, res) => {
        res.send(' ***Open new Conversation***')
    }
);

// ***Send new massage***
router.post('/:id/:massage', (req, res) => {
       res.send('***Send new massage***')
    }
);

// ------ API PUT METHODS -------


// ------ API DELETE METHODS -------
// ***Delete specific conversation***
router.delete('/:id', (req, res) => {
        res.send('***Delete specific conversation***')
    }
);

// ***Delete specific massage***
router.delete('/:id/:id', (req, res) => {
        res.send('***Delete specific massage***')
    }
);

// ------ API UTILS -------

function validateFile(file) {
    const schema = Joi.object({
        name: Joi.string().min(3).required(),
        type: Joi.string().required()
    });
    const result = schema.validate(file);
    return result
}

module.exports = router;