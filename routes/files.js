const express = require('express');
const filesService = require('../services/filesService');
const usersService = require('../services/usersService');
const router = express.Router();

// ------ API GET METHODS -------
// ***Get all files - can sorted by any attribute***
router.get('/', async (req, res) => {
    await filesService.getAllFiles(req, res);
});

// ***Get file with spec type***
router.get('/:screenId/:userId', async (req, res) => {
    await filesService.getFilesByScreenAndUser(req, res);
});

// ***Get specific file***
router.get('/:id', async (req, res) => {
    await filesService.getFileById(req, res);
});

// ------ API POST METHODS -------
// ***Save files to DB***
router.post('/:screenId/:userId',async (req, res) => {
    await usersService.verifyConnection({screenId: req.params.screenId, userId: req.params.userId});
    if (!usersService)
        res.status(400).send("user " + req.params.userId + " not connected to the screen" + req.params.screenId);
    await filesService.addNewFile(req, res);
});

// ------ API PUT METHODS -------
// ***Update specific file***
router.put('/:id', async (req, res) => {
    await filesService.updateFile(req, res);
})

// ------ API DELETE METHODS -------
// ***Delete specific file***
router.delete('/:id', async (req, res) => {
    await filesService.deleteFile(req, res)
})

// ***Delete all files***
router.delete('/', async (req, res) => {
    await filesService.deleteAllFiles(req, res)
})

// ------ API UTILS -------

module.exports = router;
