const express = require('express');
const router = express.Router();
const ordersService = require('../services/ordersService');
const productsService = require('../services/productsService');

// ------ DATA -------

// ------ API GET METHODS -------
// ***Get all orders***
router.get('/orders/:userId', async (req, res) => {
        await ordersService.getAllOrders(req, res);
    }
);

// ***Get all orders by user and screen***
router.get('/orders/:screenId/:userId', async (req, res) => {
        await ordersService.getOrderByUserAndScreen(req, res);
    }
);

// ***Get product by Id***
router.get('/products/:id', async (req, res) => {
        await productsService.getProductById(req, res);
    }
);

// ***Get all products***
router.get('/products', async (req, res) => {
        await productsService.getAllProducts(req, res);
    }
);

// ------ API POST METHODS -------
// ***Add new product***
router.post('/products/:userId', async (req, res) => {
        await productsService.createNewProduct(req, res);
    }
);

// ***Post new order***
router.post('/orders', async (req, res) => {
        await ordersService.createNewOrder(req, res);
    }
);

// ------ API PUT METHODS -------
// ***Update specific product***
router.put('/products/:id/:userId', async (req, res) => {
        await productsService.updateProductById(req, res);
    }
);

// ***Update specific order***
router.put('/orders/:id', async (req, res) => {
        await ordersService.updateOrderById(req, res);
    }
);

// ------ API DELETE METHODS -------
// ***Delete specific product***
router.delete('/products/:id/:userId', async (req, res) => {
        await productsService.deleteProductById(req, res);
    }
);

// ***Delete specific order***
router.delete('/orders/:id', async (req, res) => {
        await ordersService.deleteOrderById(req, res);
    }
);

// ***Delete order by user and screen***
router.delete('/orders/:screenId/:userId', async (req, res) => {
        await ordersService.deleteOrdersByUserAndScreen(req, res);
    }
);

module.exports = router;