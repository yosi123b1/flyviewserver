const express = require('express');
const router = express.Router();
const usersService = require('../services/usersService');

// ------ API GET METHODS -------
// ***Screen Start listen to connection***
router.get('/listen/:screenId',async (req, res) => {
    await usersService.useServerSentEventsMiddleware(req, res, usersService.listenForConnecting)
    }
);

// ***On User Click ScreenId***
router.get('/:screenId', async (req, res) => {
    await usersService.onConnectionRequest(req, res);
});

// ***Connect User To Screen***
router.get('/:screenId/:screenCode', async (req, res) => {
        await usersService.connectToScreen(req, res)
    }
);

// ------ API POST METHODS -------
// ***Create New Screen***
router.post('/:screenId', async (req, res) => {
        await usersService.createNewScreenData(req, res)
    }
);

// ------ API PUT METHODS -------


// ------ API DELETE METHODS -------
// ***Disconnect And Delete User***
router.delete('/:userId/:screenId',async (req, res) => {
        await usersService.disconnect(req,res)
    }
);

module.exports = router;