const {FileData, validateFile, validateUpdate} = require('../data/entities/fileData');
const baseUrl = require('./utils/baseUrl');
const colors = require('colors');
const fs = require('fs');
const AdmZip = require('adm-zip');

async function getAllFiles(req, res) {
    const files = await FileData.find();
    return res.send(files)
}

async function getFilesByScreenAndUser(req, res) {
    await FileData.find({screenId: req.params.screenId, userId: req.params.userId})
        .then(async files => {
            // let filesToReturn = []
            const zip = new AdmZip();
            for (let i = 0; i < files.length; i++) {
                zip.addLocalFile(files[i].path);
            }
            const downloadName = `zipFiles.zip`;
            const data = zip.toBuffer();
            // save file zip in root directory
            zip.writeZip(`${baseUrl.filesBaseUrl}${req.params.screenId}/${req.params.userId}/${downloadName}`);
            // // code to download zip file
            res.set('Content-Type','application/octet-stream');
            res.set('Content-Disposition',`attachment; filename=${downloadName}`);
            res.set('Content-Length',data.length);
            res.send({data:data});

            // res.sendFile(files[0].path)
            // res.sendFile(`${baseUrl.filesBaseUrl}${req.params.screenId}/${req.params.userId}/${downloadName}`)
            // res.zip(filesToReturn);
        })
        .catch((err) => {
            console.log(colors.red(err.message))
            res.status(404).send(`There is no file with type ${req.params.type}`)
        });
}

async function getFileById(req, res) {
    await FileData.findById(req.params.id)
        .then(file => {
            res.send(file)
        })
        .catch(() => {
            console.log(colors.red(`There is no files with id ${req.params.id}`))
            res.status(404).send(`There is no files with id ${req.params.id}`)
        });
}

async function addNewFile(req, res) {
    const {error} = validateFile(req.files.file);
    if (error) res.status(400).send(error.details[0].message);

    const filePath = `${baseUrl.filesBaseUrl}${req.params.screenId}/${req.params.userId}/${req.files.file.name}`;
    let file = new FileData({
        name: req.files.file.name,
        userId: req.params.userId,
        screenId: req.params.screenId,
        path: filePath,
    });
    try {
        file.save()
            .then(file => {
                FileData.findById(file._id, (err, findOutFile) => {
                    if (err) throw err;
                    try {
                        const myFile = req.files.file
                        myFile.mv(findOutFile.path, function (err) {
                            if (err) {
                                console.log(err)
                                return res.status(500).send({msg: "Error occured"});
                            }
                            return res.send({name: myFile.name, path: findOutFile.path});
                        });
                    } catch (e) {
                        console.log(colors.red(e))
                    }
                });
            }).catch(err => {
            console.log(colors.red(err))
            throw err;
        });
    } catch (err) {
        console.log(colors.red(err.message))
        return res.status(400).send(err.message);
    }
}

async function updateFile(req, res) {
    const {error} = validateUpdate(req.body);
    if (error) {
        res.status(400).send(error.details[0].message);
        return false;
    }

    await FileData.findById(req.params.id).then(file => {
        if (req.body.name)
            file.name = req.body.name;
        if (req.body.data)
            file.data = req.body.data;
        file.save().then(file => {
            return res.send(file);
        }).catch(err => {
            console.log(colors.red(`${err.message}`))
            return res.status(404).send(`${err.message}`);
        });
    }).catch(err => {
        console.log(err.message)
        return res.status(404).send(`The file with id ${req.params.id} is not exists!`);
    });
}

async function deleteFile(req, res) {
    await FileData.findByIdAndRemove(req.params.id).then(file => {
        res.send(file)
    }).catch(() => {
        console.log(colors.red(`The file with id ${req.params.id} is not exists!`))
        res.status(404).send(`The file with id ${req.params.id} is not exists!`);
    });
}

async function deleteAllFiles(req, res) {
    await FileData.deleteMany().then(files => {
        res.send(files)
    }).catch(err => {
        console.log(colors.red(err.message));
        res.status(404).send(err.message);
    });
}

async function removeFilesByScreenAndUser(args) {
    const screenId = args.screenId;
    const userId = args.userId;
    await FileData.find({screenId: screenId, userId: userId})
        .then(files => {
            files.forEach(file => {
                fs.unlink(file.path, () => {
                    file.remove();
                })
            })
            if (args.res != null)
                args.res.send(files);
        })
        .catch((err) => {
            console.log(colors.red(err.message));
            if (args.res != null)
                args.res.status(404).send(`There is no files of user ${userId} in screen ${screenId}`)
        });
}

module.exports = {
    getAllFiles,
    getFilesByScreenAndUser,
    getFileById,
    addNewFile,
    updateFile,
    deleteFile,
    deleteAllFiles,
    removeFilesByScreenAndUser
}
