const {OrderData, validateOrder, validateUpdatedOrder} = require('../data/entities/orderData');
const usersService = require('../services/usersService');
const userRole = require('../data/utils/userRole');
const colors = require('colors');
const {ProductData} = require("../data/entities/productData");

async function getAllOrders(req, res) {
    let role = await usersService.getUserRole({userId: req.params.userId})
    if (role != userRole.admin && role != userRole.crew)
        return res.status(400).json({message: "just admin or crew can see all orders"});

    const orders = await OrderData.find();
    return res.send(orders)
}

async function getOrderByUserAndScreen(req, res) {
    await OrderData.find({screenId: req.params.screenId, userId: req.params.userId})
        .then(orders => {
            res.send(orders)
        })
        .catch((err) => {
            console.log(colors.red(err.message));
            res.status(404).json({message: `There is no orders of user ${req.params.userId}`})
        });
}

async function createNewOrder(req, res) {
    const userValidate = await usersService.verifyConnection({userId: req.body.userId, screenId: req.body.screenId})
    if (!userValidate)
        return res.status(400).json({message: "user in not connected to the screen"});

    const {error} = validateOrder(req.body);
    if (error) return res.status(400).send(error.details[0].message);

    const productsUpdated = await checkAndOrderProducts({products:req.body.products});
    if (productsUpdated.error)
        return res.status(400).json({message: productsUpdated.message});

    let order = new OrderData({
        userId: req.body.userId,
        screenId: req.body.screenId,
        products: req.body.products,
        timeStamp: new Date().getTime()
    })
    try {
        order.save()
            .then(order => {
                return res.send(order);
            })
    } catch (err) {
        console.log(colors.red(err.message));
        return res.status(400).send(err.message);
    }
}

async function updateOrderById(req, res) {
    await OrderData.findById(req.params.id).then(async order => {
        if (req.body.userId != order.userId || req.body.screenId != order.screenId)
            return res.status(400).json({message: "user cannot edit order of someone else"});

        const {error} = validateUpdatedOrder(req.body);
        if (error) return res.status(400).send(error.details[0].message);

        updateProductsAfterCancelOrder({orders:[order]});

        if (req.body.products != null) {
            const productsUpdated = await checkAndOrderProducts({products:req.body.products})
            if (productsUpdated.error)
                return res.status(400).json({message: productsUpdated.message});
            order.products = req.body.products
        }
        order.save().then(order => {
            return res.send(order);
        }).catch(err => {
            console.log(colors.red(err.message));
            return res.status(404).send(`${err.message}`);
        });
    }).catch(err => {
        console.log(colors.red(err.message));
        return res.status(404).send(`order with id ${req.body.id} is not exists!`);
    });
}

async function deleteOrderById(req, res) {
    await OrderData.findByIdAndRemove(req.params.id).then(order => {
        order.products.forEach(productId => {
            ProductData.findById(productId).then(product=>{
                product.amount += 1;
                product.isAvailable = true;
                product.save()
            })
        })
        res.send(order)
    }).catch((err) => {
        console.log(colors.red(err.message));
        res.status(404).json({message: `Order with id ${req.params.id} is not exists!`});
    });
}

async function deleteOrdersByUserAndScreen(req, res) {
    await OrderData.find({userId: req.params.userId, screenId: req.params.screenId}).then(orders => {
        updateProductsAfterCancelOrder({orders:orders});
        orders.forEach(order=>order.remove())
        res.send(orders)
    }).catch((err) => {
        console.log(colors.red(err.message));
        res.status(404).json({message: `Orders of user ${req.params.userId} and screen ${req.params.screenId} dose not exists!`});
    });
}

async function deleteAllOrders(req, res) {
    let role = await usersService.getUserRole({userId: req.params.userId})
    if (role != userRole.admin)
        return res.status(400).json({message: "user role must be admin for this action"})
    await OrderData.deleteMany().then(orders => {
        updateProductsAfterCancelOrder(orders);
        res.send(orders)
    }).catch(err => {
        console.log(colors.red(err.message));
        res.status(404).json({message: err.message});
    });
}

async function checkAndOrderProducts(args){
    let promises = []
    args.products.forEach(productId => {
        promises.push(ProductData.findById(productId))
    })
    let products = await Promise.all(promises);
    for (let i = 0; i < products.length; i++) {
        const prod = products[i];
        if (prod != null) {
            if (!prod.isAvailable || prod.amount === 0)
                return {error: true, message: `product ${prod.name} is not available`};
            prod.amount -= 1;
            if (prod.amount === 0)
                prod.isAvailable = false;
            prod.save();
        } else {
            return {error: true, message: "some of the products are not available"};
        }
    }
    return {error:false};
}

function updateProductsAfterCancelOrder(args){
    const orders = args.orders;
    for (const order of orders){
        order.products.forEach(productId => {
            ProductData.findById(productId).then(product=>{
                product.amount += 1;
                product.isAvailable = true;
                product.save()
            })
        })
    }
}

module.exports = {
    createNewOrder,
    getAllOrders,
    getOrderByUserAndScreen,
    deleteOrderById,
    deleteOrdersByUserAndScreen,
    updateOrderById,
    deleteAllOrders
}