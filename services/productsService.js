const {ProductData, validateProduct, validateUpdatedProduct} = require('../data/entities/productData');
const userRole = require('../data/utils/userRole');
const usersService = require('../services/usersService');
const colors = require('colors');

async function getAllProducts(req, res) {
    const products = await ProductData.find();
    let availableProducts = [];
    for (let i=0;i < products.length; i++) {
        if (products[i].isAvailable)
            availableProducts.push(products[i]);
    }
    return res.send(availableProducts)
}

async function getProductById(req, res) {
    await ProductData.findById(req.params.id)
        .then(product => {
            res.send(product)
        })
        .catch((err) => {
            console.log(colors.red(err.message));
            res.status(404).json({message:`There is no product with id ${req.params.id}`})
        });
}

async function createNewProduct(req, res) {
    const {error} = validateProduct(req.body);
    if (error) return res.status(400).json({message: error.details[0].message});

    let role = await usersService.getUserRole({userId: req.params.userId})
    if (role != userRole.crew)
        return res.status(400).json({message: "Only crew member can add products"});

    let product = new ProductData({
        name: req.body.name,
        price: req.body.price,
        amount: req.body.amount,
        isAvailable: req.body.isAvailable,
        description: req.body.description,
        imageUrl: req.body.imageUrl
    })
    try {
        product.save()
            .then(product => {
                res.send(product);
            })
    } catch (err) {
        console.log(colors.red(err.message));
        return res.status(400).send(err.message);
    }
}

async function updateProductById(req, res) {
    const {error} = validateUpdatedProduct(req.body);
    if (error) return res.status(400).json({message: error.details[0].message});

    let role = await usersService.getUserRole({userId: req.params.userId})
    if (role != userRole.crew)
        return res.status(400).json({message: "Only crew member can update products"});

    await ProductData.findById(req.params.id).then(product => {
        if (req.body.price)
            product.price = req.body.price;
        if (req.body.amount)
            product.amount = req.body.amount;
        if (req.body.isAvailable)
            product.isAvailable = req.body.isAvailable;
        if (req.body.description)
            product.description = req.body.description;
        product.save().then(product => {
            return res.send(product);
        }).catch(err => {
            return res.status(404).send(`${err.message}`);
        });
    }).catch(err => {
        console.log(colors.red(err.message));
        return res.status(404).send(`product with id ${req.params.id} is not exists!`);
    });
}

async function deleteProductById(req, res) {
    let role = await usersService.getUserRole({userId: req.params.userId})
    if (role != userRole.crew)
        return res.status(400).json({message: "Only crew member can delete specific product"});

    await ProductData.findByIdAndRemove(req.params.id).then(product => {
        res.send(product)
    }).catch((err) => {
        console.log(colors.red(err.message));
        res.status(404).json({message:`The product with id ${req.params.id} is not exists!`});
    });
}

async function deleteAllProducts(req, res) {
    let role = await usersService.getUserRole({userId: req.params.userId})
    if (role != userRole.admin)
        return res.status(400).json({message: "Only admin can remove all products"});

    await ProductData.deleteMany().then(products => {
        res.send(products)
    }).catch(err => {
        console.log(colors.red(err.message));
        res.status(404).json({message:err.message});
    });
}

module.exports = {
    createNewProduct,
    getAllProducts,
    getProductById,
    deleteProductById,
    updateProductById,
    deleteAllProducts
}
