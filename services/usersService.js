const {UserData} = require('../data/entities/userData');
const {ScreenData} = require('../data/entities/screenData');
const filesService = require('./filesService');
const baseUrl = require('./utils/baseUrl');
const userRole = require('../data/utils/userRole');
const fs = require('fs');
const colors = require('colors');

async function getAllUsers(req, res) {
    let role = await getUserRole({userId: req.params.userId})
    if (role != userRole.admin)
        return res.status(400).json({message: "user role must be admin for this action"})

    const users = await UserData.find();
    return res.send(users)
}

async function getAllScreens(req, res) {
    let role = await getUserRole({userId: req.params.userId})
    if (role != userRole.admin)
        return res.status(400).json({message: "user role must be admin for this action"})
    const screens = await ScreenData.find();
    return res.send(screens)
}

async function onConnectionRequest(req, res) {
    let screen = await ScreenData.findOne({screenId: req.params.screenId})
    if (!screen) return res.status(400).json({message:'Screen is not exists', status:400 });
    screen.connectRequest = true;
    try {
        await screen.save();
        return res.status(200).json({code: screen.screenCode, status:200});
    } catch (err) {
        console.log(colors.red(err.message));
        return res.status(400).json({message: err.message, status:400 });
    }
}

async function verifyConnection(args) {
    const screenId = args.screenId;
    const userId = args.userId;
    let user = await UserData.findOne({userId: userId})
    if (!user)
        return false;
    if (screenId != user.screenId)
        return false;
    return true;
}

async function createUser(args) {
    const userId = Math.random().toString(36).slice(-8);
    const userData = new UserData({
        userId: userId,
        role: args.role,
        name: args.name,
        screenId: args.screenId
    })
    try {
        const user = await userData.save();
        return user;
    } catch (e) {
        console.log(colors.red(e.message));
        return null
    }
}

async function getUserRole(args) {
    let user = await UserData.findOne({userId: args.userId})
    if (!user)
        return null
    return user.role
}

async function connectToScreen(req, res) {
    let screen = await ScreenData.findOne({screenId: req.params.screenId})
    if (!screen)
        return res.status(400).json({message: 'Screen is not exists', status:400});

    if (!screen.connectRequest || screen.isConnected)
        return res.status(400).json({message: `cannot connect to screen ${screen.screenId}`,status:400 });

    if (req.params.screenCode != screen.screenCode)
        return res.status(400).json({message:'Invalid Screen Code',status:400} );

    const userData = await createUser({screenId: screen.screenId, name: req.params.name})
    if (!userData)
        return res.status(400).json({message: 'Cannot create user!',status:400 });

    screen.isConnected = true;
    screen.userId = userData.userId;
    try {
        await screen.save();
        fs.mkdir(`${baseUrl.filesBaseUrl}${screen.screenId}/${userData.userId}`, () => {
            res.send(userData)
        });
    } catch (err) {
        console.log(colors.red(err.message));
        return res.status(400).json({message: err.message,status:400 });
    }
}

async function createNewScreenData(req, res) {
    let screenId = req.params.screenId

    let screenData = await ScreenData.findOne({screenId: screenId})
    if (screenData)
        return res.status(400).send('Screen is already exists');

    // let screenCode = Math.random().toString(36).slice(-8);
    let screenCode = "123456";

    screenData = new ScreenData({
        screenId: screenId,
        userId: "",
        screenCode: screenCode,
        connectRequest: false,
        isConnected: false
    });
    try {
        await screenData.save();
        fs.mkdir(`${baseUrl.filesBaseUrl}${screenData.screenId}`, () => {
            res.send(screenData);
        });
    } catch (err) {
        console.log(colors.red(err.message));
        return res.status(400).send(err.message);
    }
}

async function useServerSentEventsMiddleware(req, res, next) {
    let screen = await ScreenData.findOne({screenId: req.params.screenId})
    if (!screen)
        return res.status(400).send("Screen is not registered yet...");

    res.setHeader('Content-Type', 'text/event-stream');
    res.setHeader('Cache-Control', 'no-cache');
    // only if you want anyone to access this endpoint
    res.setHeader('Access-Control-Allow-Origin', '*');

    res.flushHeaders();

    const sendEventStreamData = (data) => {
        const sseFormattedResponse = `data: ${JSON.stringify(data)}\n\n`;
        res.write('event: ping\n');  // message event
        res.write(sseFormattedResponse);
    }

    // we are attaching sendEventStreamData to res, so we can use it later
    Object.assign(res, {
        sendEventStreamData
    });
    next(req, res)
}

async function listenForConnecting(req, res) {
    let connectInterval = setInterval(async () => {
        let screen = await ScreenData.findOne({screenId: req.params.screenId})
        if (screen.connectRequest) {
            const data = {
                screenId: screen.screenId,
                code: screen.screenCode,
                userId : screen.userId,
                connectRequest: screen.connectRequest,
                isConnected: screen.isConnected
            };
            res.sendEventStreamData(data);
        }
        if (screen.isConnected) {
            clearInterval(connectInterval);
            res.end();
            // listenForDisconnecting(req, res);
        }
    }, 2000);
}

function listenForDisconnecting(req, res) {
    let disConnectInterval = setInterval(async () => {
        let screen = await ScreenData.findOne({screenId: req.params.screenId})
        if (!screen.isConnected) {
            const data = {
                code: screen.screenCode,
                connectRequest: screen.connectRequest,
                isConnected: screen.isConnected
            };
            res.sendEventStreamData(data);
            clearInterval(disConnectInterval);
            res.end();
        }
    }, 5000);
}

async function disconnectAllUsers(req, res) {
    let role = await getUserRole({userId: req.params.userId})
    if (role != userRole.admin)
        return res.status(400).json({message: "user role must be admin for this action"})

    await ScreenData.find().then(screens => {
        screens.forEach(screen => {
            screen.isConnected = false;
            screen.connectRequest = false;
            try {
                screen.save();
            } catch (e) {
                console.log(colors.red(e.message));
                return res.status(400).send(e.message);
            }
        })
    });
    const users = await UserData.find();
    users.forEach(user=>{
        if (user.role === userRole.passenger) {
            filesService.removeFilesByScreenAndUser({"screenId": user.screenId, "userId": user.userId}).then(() => {
                fs.rmdir(`${baseUrl.filesBaseUrl}${user.screenId}/${user.userId}`, () => {
                    user.remove();
                });
            }).catch((e) => {
                console.log(colors.red(e.message));
                return res.status(404).send(e.message);
            });
        }
    })
    return res.status(200).json({message: "success"});
}

async function disconnect(req, res) {
    let user = await UserData.findOne({userId: req.params.userId})
    let screen = await ScreenData.findOne({screenId: req.params.screenId})

    if (!user || !screen)
        return res.status(400).send('User Or screen is not found');
    if (!user.screenId == screen.screenId)
        return res.status(400).send('User is not connected to this screen');
    if (!screen.isConnected)
        return res.status(400).send('Screen is not connected');

    screen.isConnected = false
    screen.connectRequest = false
    try {
        screen.save();
    } catch (e) {
        console.log(colors.red(e.message));
        res.status(400).send(e.message);
    }
    user.remove().then(async (user) => {
        filesService.removeFilesByScreenAndUser({"screenId": screen.screenId, "userId": user.userId}).then(() => {
            fs.rmdir(`${baseUrl.filesBaseUrl}${screen.screenId}/${user.userId}`, () => {
                res.send(user);
            });
        });
    }).catch((e) => {
        console.log(colors.red(e.message));
        res.status(404).send(e.message);
    });
}

module.exports = {
    getAllUsers,
    getAllScreens,
    onConnectionRequest,
    createNewScreenData,
    createUser,
    getUserRole,
    verifyConnection,
    connectToScreen,
    useServerSentEventsMiddleware,
    listenForConnecting,
    disconnect,
    disconnectAllUsers
}
